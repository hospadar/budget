package com.luke.budget;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import java.util.ArrayList;

public class EnterExpenseActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    Transactions transactions;

    final Context context = this;



    @Override
    public void onCreate(Bundle savedInstanceState) {

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        super.onCreate(savedInstanceState);


        setContentView(R.layout.main);
        transactions = new Transactions(this);

        final AutoCompleteTextView textView = (AutoCompleteTextView)
                findViewById(R.id.expense_name);

        textView.setAdapter(transactions.getAutoSuggestAdapter(this));

        textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                textView.showDropDown();
                Button b = (Button)findViewById(R.id.expense_show_dropdown);
                evaluateDropdownButton();
            }
        });

        textView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                Log.w("budget", "onfocus");
            }
        });


        textView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                hideKeypad();
                evaluateDropdownButton();
            }
        });



        findViewById(R.id.expense_show_dropdown).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Button b = (Button) view;
                AutoCompleteTextView expense_name = (AutoCompleteTextView)findViewById(R.id.expense_name);

                if ("v".equals(b.getText())){
                    expense_name.showDropDown();
                } else {
                    expense_name.dismissDropDown();
                }
                evaluateDropdownButton();
            }
        });

        //Log.w("budget", "setting up keypad");
        KeypadBuilder keypad = new KeypadBuilder(this, R.id.expense_price);


    }

    public void hideKeypad(){
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(findViewById(R.id.expense_name).getWindowToken(), 0);

    }



    public void evaluateDropdownButton(){
        Button b = (Button)findViewById(R.id.expense_show_dropdown);
        AutoCompleteTextView expense_name = (AutoCompleteTextView)findViewById(R.id.expense_name);
        if(!expense_name.isPopupShowing() ){
            b.setText("v");
        }
        else {
            b.setText("^");
        }
    }
}
