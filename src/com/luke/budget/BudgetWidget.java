package com.luke.budget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.util.Pair;
import android.widget.RemoteViews;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lhospadaruk on 3/21/14.
 */
public class BudgetWidget extends AppWidgetProvider {

    private static final String doubleFormat = "%.0f";
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd");

    private static Double getBudgetTotal(Context context){
        SharedPreferences preferences = context.getSharedPreferences("com.luke.budget_preferences", 0);

        Double budget_amount;

        try{
            budget_amount = Double.parseDouble(preferences.getString("pref_key_budget_amount", "150.0"));
        } catch (NumberFormatException e){
            budget_amount = 150.;
        }

        return budget_amount;
    }

    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        doUpdate(context, appWidgetManager, appWidgetIds);
    }


    private static void doUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds){
        Transactions transactions = new Transactions(context);
        for(int widgetId : appWidgetIds){
            Intent intent = new Intent(context, EnterExpenseActivity.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
            RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_layout);
            views.setOnClickPendingIntent(R.id.widget_budget, pendingIntent);

            appWidgetManager.updateAppWidget(widgetId, views);

            Double total = getBudgetTotal(context);
            Double spent = transactions.getBudgetSpent();

            views.setTextViewText(R.id.widget_budget_total, String.format(doubleFormat, total));

            views.setTextViewText(R.id.widget_budget_spent, String.format(doubleFormat, spent));

            if ( spent.compareTo(total) <= 0){
                views.setInt(R.id.widget_budget_spent, "setTextColor", Color.GREEN);
            } else {
                views.setInt(R.id.widget_budget_spent, "setTextColor", Color.RED);
            }



            Pair<Date, Date> dates = transactions.getDates();

            views.setTextViewText(R.id.widget_budget_daysleft,
                    ((Integer)((Double) ((Math.floor(dates.second.getTime() - (new Date()).getTime())) / 1000 / 60 / 60 / 24 + 1.0)).intValue()).toString());

            views.setTextViewText(R.id.widget_budget_date, dateFormat.format(dates.second));


            appWidgetManager.updateAppWidget(widgetId, views);
        }
    }

    public static void doUpdate(Context context){
        ComponentName name = new ComponentName(context, BudgetWidget.class);
        AppWidgetManager manager = AppWidgetManager.getInstance(context);
        int[] appWidgetIds = manager.getAppWidgetIds(name);
        doUpdate(context, manager, appWidgetIds);

    }
}
