package com.luke.budget;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Loader;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

/**
 * Created by lhospadaruk on 3/24/14.
 */
public class TransactionViewer extends ListActivity{

    CursorAdapter mAdapter;
    Transactions mTransactions;

    final ListActivity context = this;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mTransactions = new Transactions(this);

        mAdapter = mTransactions.getTransactionAdapter(this);

        setListAdapter(mAdapter);


        getListView().setOnItemLongClickListener(
                new AdapterView.OnItemLongClickListener() {
                    @Override
                    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

                        final View originalView = view;

                        View clone =  getLayoutInflater().inflate(R.layout.individual_transaction, null);

                        for (int fieldid: new int[]{R.id.transaction_view_rowid, R.id.transaction_view_time, R.id.transaction_view_price, R.id.transaction_view_name}){
                            ((TextView)clone.findViewById(fieldid))
                                    .setText(((TextView)view.findViewById(fieldid)).getText());
                        }



                        new AlertDialog.Builder(context)
                                .setTitle("Are you sure you want to delete this transaction?")
                                .setView(clone)
                                .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        mTransactions.deleteTransaction(
                                                ((TextView)originalView.findViewById(R.id.transaction_view_rowid)).getText().toString());

                                        mAdapter.swapCursor(mTransactions.getTransactionAdapter(context).getCursor());
                                        BudgetWidget.doUpdate(context);
                                    }
                                })
                                .setNegativeButton(android.R.string.cancel, null)
                                .show();

                        return false;
                    }
                }
        );
    }

}
