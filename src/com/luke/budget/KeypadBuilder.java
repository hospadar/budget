package com.luke.budget;

import android.app.Activity;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by lhospadaruk on 3/19/14.
 */
class KeypadBuilder {

    Integer textFieldId;
    final EnterExpenseActivity root;
    KeypadButtonListener listener;



    public KeypadBuilder(EnterExpenseActivity rootActivity, Integer textFieldIdParam){
        //Log.w("budget", "inside keypad constructor");

        textFieldId = textFieldIdParam;
        root = rootActivity;

        //listener for numeric buttons and decimal
        listener = new KeypadButtonListener();
        listener.setupKeypadListener(this.root, this.textFieldId);
        //add listener to buttons
        //Log.w("budget", "setting up number buttons");
        ViewGroup rootView = (ViewGroup)root.getWindow().getDecorView().findViewById(android.R.id.content);
        setupButtons(rootView);

        //setup listener for negative button
        root.findViewById(R.id.keypad_negative_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                root.hideKeypad();
                root.evaluateDropdownButton();
                EditText price = ((EditText)root.findViewById(textFieldId));
                String current_price = price.getText().toString();
                if (current_price.startsWith("-")){
                    current_price = current_price.substring(1);
                } else {
                    current_price = "-"+current_price;
                }
                price.setText(current_price);
            }
        });

        //setup listener for clear button
        root.findViewById(R.id.keypad_clear).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                root.hideKeypad();
                root.evaluateDropdownButton();
                ((EditText)root.findViewById(textFieldId)).setText("");
            }
        });

        //Backspace listener
        root.findViewById(R.id.keypad_backspace).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                root.hideKeypad();
                root.evaluateDropdownButton();
                EditText textField = ((EditText)root.findViewById(textFieldId));
                if (textField.getText() != null && textField.getText().length() > 0){
                    textField.setText(textField.getText().subSequence(0,textField.length()-1));
                }
            }
        });

        //Close listener that saves transactions
        root.findViewById(R.id.keypad_OK).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                root.hideKeypad();
                root.evaluateDropdownButton();
                String name = ((EditText)root.findViewById(R.id.expense_name)).getText().toString();
                String priceString = ((EditText)root.findViewById(R.id.expense_price)).getText().toString();
                try{
                    Double price = Double.parseDouble(priceString);
                    root.transactions.putExpense(name, price);
                    BudgetWidget.doUpdate(root);
                    root.finish();
                } catch (NumberFormatException e){
                    ((TextView)root.findViewById(R.id.error_area)).setText(R.string.bad_float);
                }


            }
        });
    }

    private void setupButtons(ViewGroup rootView){
        for( int i=0; i<rootView.getChildCount(); i++){

            View child = rootView.getChildAt(i);

            if ("keypad_button".equals(child.getTag())){
                Button b = (Button)child;
                b.setOnClickListener( listener );
            }
            else if (ViewGroup.class.isAssignableFrom(child.getClass())){
                setupButtons((ViewGroup)child);
            }
        }
    }

     class KeypadButtonListener implements View.OnClickListener {

        EnterExpenseActivity root = null;
        Integer textFieldId = null;
        Pattern decimalFinder = Pattern.compile(".*\\.[0-9]{2,}$");

        public void setupKeypadListener(EnterExpenseActivity root, Integer textFieldId){
            this.root = root;
            this.textFieldId = textFieldId;
        }

        @Override
        public void onClick(View view) {

            root.hideKeypad();
            root.evaluateDropdownButton();

            Button b = (Button) view;

            try{
                //Log.w("budget", "inside numeric button callback");
                EditText textField = (EditText)root.findViewById(textFieldId);

                //check for extraneous decimals
                String currentPrice = textField.getText().toString();
                if (currentPrice.contains(".") && ".".equals(b.getText())){
                    return;
                }
                //check for too many numbers after the decimal
                else if (currentPrice.contains(".") && decimalFinder.matcher(currentPrice).matches()){
                    return;
                }

                textField.setText(currentPrice+b.getText().toString());

            } catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
