package com.luke.budget;

import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.util.Pair;
import android.widget.CursorAdapter;
import android.widget.FilterQueryProvider;
import android.widget.ResourceCursorAdapter;
import android.widget.SimpleCursorAdapter;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by lhospadaruk on 3/19/14.
 */
public class Transactions extends SQLiteOpenHelper{
    private static final int DB_VERSION = 6;
    private static final String DB_NAME = "budget_transactions";
    private static final String DB_TRANSACTIONS = "transactions";
    private static final String DB_SUGGESTIONS = "suggestions";
    private static final String[] DB_CREATE = new String[]{
            "CREATE TABLE IF NOT EXISTS " + DB_TRANSACTIONS + " ( _id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT, price, time TEXT);",
            "CREATE INDEX IF NOT EXISTS timeidx ON "+DB_TRANSACTIONS+"(time);",
            "CREATE INDEX IF NOT EXISTS nameidx ON "+DB_TRANSACTIONS+"(name);",
            "CREATE TABLE IF NOT EXISTS "+ DB_SUGGESTIONS +" (_id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT UNIQUE, count INTEGER DEFAULT 0);",
            "CREATE INDEX IF NOT EXISTS suggestionidx ON "+DB_SUGGESTIONS+"(name);"};

    public static final SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy-MM-dd - HH:mm"){{setTimeZone(TimeZone.getDefault());}};


    public static final String DEFAULT_NAME = "no_name";

    private Context context;

    Transactions(Context context){
        super(context, DB_NAME, null, DB_VERSION );

        this.context = context;

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        for (String command: DB_CREATE){
            sqLiteDatabase.execSQL(command);
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i2) {
        onCreate(sqLiteDatabase);
    }

    public CursorAdapter getAutoSuggestAdapter(Context context){
        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                context,
                android.R.layout.simple_dropdown_item_1line,
                getReadableDatabase().rawQuery("SELECT _id, name, count FROM suggestions ORDER BY count DESC LIMIT 5;", null),
                new String[]{"name"},
                new int[]{android.R.id.text1},
                0
        );

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                if (charSequence == null || charSequence.toString().trim().length() == 0){
                    return getReadableDatabase().rawQuery("select _id, name, count from suggestions order by count desc LIMIT 5;", null);
                }
                else {
                    return getReadableDatabase().rawQuery("select _id, name, count from suggestions where name like ? order by count desc LIMIT 5;",
                            new String[]{charSequence.toString().trim().toLowerCase() + "%"});
                }
            }
        });



        adapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                return cursor.getString(cursor.getColumnIndex("name"));
            }
        });

        return adapter;
    }

    public CursorAdapter getTransactionAdapter(Context context){

        SimpleCursorAdapter adapter = new SimpleCursorAdapter(
                context,
                R.layout.individual_transaction,
                getReadableDatabase().rawQuery("SELECT _id, name, price, time FROM transactions ORDER BY time DESC;", null),
                new String[]{"_id", "name", "price", "time"},
                new int[]{R.id.transaction_view_rowid, R.id.transaction_view_name, R.id.transaction_view_price, R.id.transaction_view_time},
                0
        );

        adapter.setFilterQueryProvider(new FilterQueryProvider() {
            @Override
            public Cursor runQuery(CharSequence charSequence) {
                if (charSequence == null || charSequence.toString().trim().length() == 0){
                    return getReadableDatabase().rawQuery("SELECT _id, name, price, time FROM transactions ORDER BY time DESC;", null);
                }
                else {
                    return getReadableDatabase().rawQuery("SELECT _id, name, price, time FROM transactions where name like ? ORDER BY time DESC;",
                            new String[]{charSequence.toString() + "%"});
                }
            }
        });


        adapter.setCursorToStringConverter(new SimpleCursorAdapter.CursorToStringConverter() {
            @Override
            public CharSequence convertToString(Cursor cursor) {
                return cursor.getString(cursor.getColumnIndex("name"));
            }
        });

        return adapter;

    }

    public void putExpense(String name, Double price){
        //trim and lowercase all names

        if (name == null || name.trim().length() == 0){
            name = DEFAULT_NAME;
        }
        name = name.toLowerCase().trim();

        Date d = new Date();
        SQLiteDatabase db = getWritableDatabase();
        //put in the transaction
        ContentValues insertValues = new ContentValues();
        insertValues.put("name", name);
        insertValues.put("price", price.toString());
        insertValues.put("time", timeFormat.format(d));
        db.insert(DB_TRANSACTIONS, null, insertValues);

        //update the count
        db.beginTransactionNonExclusive();
        try{
            Cursor c = db.query(
                    DB_SUGGESTIONS,
                    new String[]{"count"},
                    "name = ?",
                    new String[]{name},
                    null,
                    null,
                    null,
                    "1"
                    );

            //if it's already there, bump it up
            if (c.getCount()>0){
                c.moveToFirst();
                Integer current_count = c.getInt(c.getColumnIndex("count"));
                current_count+=1;

                ContentValues updateVal = new ContentValues();
                updateVal.put("count", current_count);

                db.update(
                        DB_SUGGESTIONS,
                        updateVal,
                        "name = ?",
                        new String[]{name}
                        );
                Log.i("budget", "'"+name+"' has now been used "+current_count.toString()+" times");
            } else {
                //if it's not there, stuff it in.
                ContentValues values = new ContentValues();
                values.put("name", name);
                values.put("count", 1);
                Long retval = db.insertWithOnConflict(DB_SUGGESTIONS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
            }
            //mark success
            db.setTransactionSuccessful();
        } catch (Exception e){
            Log.e("budget", "Couldn't update suggestions table, exc msg: " + e.getMessage());
        } finally {
            db.endTransaction();
        }
    }



    public Pair<Date, Date> getDates(){

        List<Integer> daysToReset = DayOfMonthPreference.getStoredDays(context);

        GregorianCalendar today = new GregorianCalendar();

        Date start = null;
        Date end = null;

        for (Integer day : daysToReset){
            if (day.compareTo(today.get(Calendar.DAY_OF_MONTH)) <= 0){
                GregorianCalendar startCalendar = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH), day);
                start = startCalendar.getTime();
            }
        }
        if (start == null){
            GregorianCalendar startCalendar = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH)-1, 1);
            int startDay = daysToReset.get(daysToReset.size()-1);
            if (startDay > startCalendar.getMaximum(Calendar.DAY_OF_MONTH)){
                startDay = startCalendar.getMaximum(Calendar.DAY_OF_MONTH);
            }
            startCalendar.set(Calendar.DAY_OF_MONTH, startDay);
            start = startCalendar.getTime();
        }
        final List<Integer> originalDays = daysToReset;
        daysToReset = new ArrayList<Integer>(){{addAll(originalDays);}};
        Collections.reverse(daysToReset);
        for (Integer day : daysToReset){
            if (day.compareTo(today.get(Calendar.DAY_OF_MONTH)) > 0){
                GregorianCalendar endCalendar = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH), day);
                end = endCalendar.getTime();
            }
        }

        if (end == null){
            GregorianCalendar endCalendar = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH)+1, 1);
            int startDay = daysToReset.get(daysToReset.size()-1);
            if (startDay > endCalendar.getMaximum(Calendar.DAY_OF_MONTH)){
                startDay = endCalendar.getMaximum(Calendar.DAY_OF_MONTH);
            }
            endCalendar.set(Calendar.DAY_OF_MONTH, startDay);
            end = endCalendar.getTime();
        }


        /*
        SharedPreferences preferences = context.getSharedPreferences("com.luke.budget_preferences", 0);

        Double budget_amount;

        Integer start_day;

        Integer budgets_per_month;

        try{
            budgets_per_month = Integer.parseInt(preferences.getString("pref_key_budgets_per_month", "2"));
        } catch (NumberFormatException e){
            budgets_per_month = 2;
        }
        Integer days_per_budget = (int)Math.round(30.34 / budgets_per_month.doubleValue());

        GregorianCalendar today = new GregorianCalendar();

        GregorianCalendar budget_start_date = new GregorianCalendar(today.get(Calendar.YEAR), today.get(Calendar.MONTH), 1);

        GregorianCalendar budget_end_date = new GregorianCalendar();
        budget_end_date.setTime(budget_start_date.getTime());

        if (budgets_per_month.equals(1)){
            budget_end_date.set(Calendar.DAY_OF_MONTH, budget_start_date.getMaximum(Calendar.DAY_OF_MONTH));
            budget_end_date.setTime(new Date(budget_end_date.getTime().getTime() + 24*60*60*1000));
        } else {
            budget_end_date.setTime(new Date(budget_start_date.getTime().getTime() + days_per_budget *24*60*60*1000));
        }


        for (int i=0; i<budgets_per_month; i++){

            if (today.getTime().after(budget_start_date.getTime()) && today.getTime().before(budget_end_date.getTime())){
                if (budget_end_date.get(Calendar.MONTH) != budget_start_date.get(Calendar.MONTH)){
                    budget_end_date.set(Calendar.MONTH, 1);
                }
                output = new Pair<Date, Date>(budget_start_date.getTime(), budget_end_date.getTime());
                break;
            }

            budget_start_date.setTime(new Date(budget_start_date.getTime().getTime() + days_per_budget*24*60*60*1000));
            //last budget should always stretch to the end of the month
            if (i == budgets_per_month-2){
                budget_end_date.set(Calendar.DAY_OF_MONTH, budget_end_date.getMaximum(Calendar.DAY_OF_MONTH));
                budget_end_date.setTime(new Date(budget_end_date.getTime().getTime() + 24*60*60*1000));
                if (budget_end_date.get(Calendar.MONTH) != budget_start_date.get(Calendar.MONTH)){
                    budget_end_date.set(Calendar.MONTH, 1);
                }
            }
            else{
                budget_end_date.setTime(new Date(budget_end_date.getTime().getTime() + days_per_budget * 24 * 60 * 60 * 1000));
                if (budget_end_date.get(Calendar.MONTH) != budget_start_date.get(Calendar.MONTH)){
                    budget_end_date.set(Calendar.MONTH, 1);
                }
            }

        }

        */


        return new Pair<Date, Date>(start, end);
    }





    public Double getBudgetSpent(){

        Pair<Date, Date> dates = getDates();
        SQLiteDatabase db = getReadableDatabase();

        Cursor c = db.rawQuery("SELECT SUM(price) as total from "+DB_TRANSACTIONS+" where time > ?", new String[]{timeFormat.format(dates.first.getTime())});

        if (c.getCount() > 0 ){
            c.moveToFirst();
            Double total = c.getDouble(c.getColumnIndex("total"));
            return  total;

        } else {
            return 0.;
        }

    }

    public void deleteTransaction(String rowid){
        SQLiteDatabase db = getWritableDatabase();
        db.delete(DB_TRANSACTIONS, "_id = ?", new String[]{rowid});
    }
}
