package com.luke.budget;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.Preference;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import java.util.*;

/**
 * Created by lhospadaruk on 9/16/14.
 */
public class DayOfMonthPreference  extends Activity {

    final DayOfMonthPreference context = this;

    private static Set<Integer> defaultDays = Collections.unmodifiableSet(new HashSet<Integer>() {{
        add(1);//8
        add(9); //7
        add(16); //8
        add(24); //7/8
    }});

    private static Set<String> defaultDayStrings = Collections.unmodifiableSet(new HashSet<String>() {{
        add("1");
        add("9");
        add("16");
        add("24");
    }});

    @Override
    public void onCreate(Bundle savedInstance){
        super.onCreate(savedInstance);
        setContentView(R.layout.month_days);

        for (Integer i : getStoredDays(context)){
            addField(i);
        }

        Button add_day = (Button)findViewById(R.id.add_day);
        add_day.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.addField(null);
            }
        });

        Button save = (Button)findViewById(R.id.save_day_preferences);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.saveDates();
                context.finish();
            }
        });
    }

    @Override
    protected void onStop(){
        saveDates();
        super.onStop();
    }

    private void addField(Integer day){
        LinearLayout l = (LinearLayout)findViewById(R.id.month_days);
        final LinearLayout newDay = (LinearLayout)((LayoutInflater)context.getSystemService(LAYOUT_INFLATER_SERVICE)).inflate(R.layout.day_of_month, l, true);
        if (day != null) {
            ((EditText) newDay.getChildAt(newDay.getChildCount() - 1).findViewById(R.id.day_of_month_preference)).setText(day.toString());
        }
        for (int i = 0; i < newDay.getChildCount(); i++){
            ((Button)newDay.getChildAt(i).findViewById(R.id.remove_day)).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View button) {
                    LinearLayout day = (LinearLayout) button.getParent();
                    ((LinearLayout) day.getParent()).removeView(day);
                }
            });
        }
    }

    private void saveDates(){
        SharedPreferences preferences = context.getSharedPreferences("com.luke.budget_preferences", 0);
        SharedPreferences.Editor prefEditor = preferences.edit();
        HashSet<String> toSave = new HashSet<String>();
        for (Integer i : getDays()){
            toSave.add(i.toString());
        }
        prefEditor.putStringSet("days_to_reset", toSave);
        prefEditor.apply();
        prefEditor.commit();
        BudgetWidget.doUpdate(context);
    }

    public static List<Integer> getStoredDays(Context reqContext){
        SharedPreferences preferences = reqContext.getSharedPreferences("com.luke.budget_preferences", 0);
        Set<String> dayStrings = preferences.getStringSet("days_to_reset", defaultDayStrings);
        ArrayList<Integer> days = new ArrayList<Integer>();
        for (String s : dayStrings){
            try{
                days.add(Integer.parseInt(s));
            } catch (NumberFormatException e){
                //eh
            }
        }
        if (days.size() < 1){
            days.addAll(defaultDays);
        }
        Collections.sort(days);
        return Collections.unmodifiableList(days);
    }

    private Set<Integer> getDays(){
        if (((LinearLayout)findViewById(R.id.month_days)).getChildCount() < 1){
            return defaultDays;
        } else{
            Set<Integer> out = new HashSet<Integer>();
            for (int i = 0; i < ((LinearLayout)findViewById(R.id.month_days)).getChildCount(); i++){
                String numberString  = ((EditText)((LinearLayout)findViewById(R.id.month_days)).getChildAt(i).findViewById(R.id.day_of_month_preference)).getText().toString();
                Integer number = null;
                try{
                    number = Integer.parseInt(numberString);
                } catch (NumberFormatException e){
                    //oh well
                }
                if (number != null && number > 0 && number < 32){
                    out.add(number);
                }
            }
            if (out.size()>0){
                return Collections.unmodifiableSet(out);
            } else {
                return defaultDays;
            }
        }
    }
}
